<?php
    class TodoList {

      public function createTask($title){
        require 'pdo.php';
        $response = $bdd->prepare('INSERT INTO tasks(title, done, dateCreation) VALUES(:title, 0, NOW())');
        $response->execute(array(
          'title' => $title
        ));
        $response->closeCursor();
        header('Location: todolistAvecClasses.php');
      }
      public function deleteTask($id){
        require 'pdo.php';
        $response = $bdd->prepare('DELETE FROM tasks where id = :id');
        $response->execute(array(
          'id' => $id
        ));
        $response->closeCursor();
      }
      public function updateTask($id){
        require 'pdo.php';
        $response = $bdd->prepare('UPDATE tasks SET done = 1 WHERE id = :id');
        $response->execute(array(
          'id' => $id
        ));
      }
    }

 ?>
