<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1"/>
    <title>todolist avec classes</title>
  </head>
<body>
  <?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);


  $bdd = new PDO('mysql:host=localhost;dbname=todo;charset=utf8',
   'todo_adm', 'todo_pass');

   ?>
  <div class="col-md-3"></div>
  <div class="col-md-6 well">
    <h3 class="text-primary">PHP - Simple To Do List App</h3>
    <hr style="border-top:1px dotted #ccc;"/>
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <form method="POST" class="form-inline" action="todolistAvecClasses.php">
          <input type="text" class="form-control" name="task"/>
          <input type="submit" class="btn btn-primary form-control" name="add" value="add task"></button>
    </div>
    <br /><br /><br />
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Task</th>
          <th>Création</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
        <?php
        include('todoClasses.class.php');
        $todo = new TodoList();

        if (!empty($_POST['task'])) {
          if (isset($_POST['add'])) {
            $todo->createTask($_POST['task']);
          }
        }

        $response = $bdd->prepare('SELECT * FROM tasks');
        $response->execute();

        $count = 1;
        while($datas = $response->fetch())
        {
          echo "<tr><td>";
          echo $count."</td>";
          echo "<td>";
          echo $datas['title']."</td>";
          echo "<td>";
          $dateAffiche = new DateTime($datas['dateCreation']);
          echo $dateAffiche->format('d-m-Y H:i:s')."</td>"."<td>";
          if ($datas['done'] == 1) {
            echo "DONE";
          } elseif ($datas['done'] == 0) {
            echo "A faire";
          } else {
            echo "ERROR";
          }
          echo "</td>";
          echo "<td>";
          if ($datas['done'] == 0) {

            echo "<input type='submit' name='delete".$datas['id']."' value='DELETE'/>";
            $tempo = "delete".$datas['id'];
            $tempoid = $datas['id'];
            if (isset($_POST[$tempo])) {
              $todo->deleteTask($tempoid);
              header('Location: todolistAvecClasses.php');
            }
            echo "<input type='submit' name='done".$datas['id']."' value='DONE'/>";
            $tempo = "done".$datas['id'];
            if (isset($_POST[$tempo])) {
              $todo->updateTask($tempoid);
              header('Location: todolistAvecClasses.php');
            }
          } elseif ($datas['done'] == 1) {
            echo "<input type='submit' name='delete".$datas['id']."' value='DELETE'/>";
            $tempo = "delete".$datas['id'];
            $tempoid = $datas['id'];
            if (isset($_POST[$tempo])) {
              $todo->deleteTask($tempoid);
              header('Location: todolistAvecClasses.php');
            }
          }

          echo "</td>";
          echo "</tr>";
          $count ++;
        }
        ?>

      </form>

    </table>

  </div>
</body>
</html>
